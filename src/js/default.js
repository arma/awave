var scrollDownElement = document.getElementById('scroll_down');
scrollDownElement.addEventListener('click', function(e) {
	e.preventDefault();
	doScrolling('#bottom_side', 1000);
});

// Sliders
var amount_slider = document.getElementById('amount_slider');
noUiSlider.create(amount_slider, {
	start: 5000,
	step: 1000,
	connect: [true, false],
	format: wNumb({
		decimals: 0
	}),
	range: {
		'min': 1000,
		'max': 20000,
	},
	pips: {
		mode: 'range',
		density: 5.26316,
	}
});
amount_slider.noUiSlider.on('update', function (values) {
	var output = document.getElementById('amount_value');
	var perMonth = document.getElementById('perMonth');
	var total = document.getElementById('total');
	var outputFormat = wNumb({
		thousand: ' ',
		suffix: ' kr'
	});

	var calc = parseInt(parseInt(values[0]) / 30);

	output.innerHTML = outputFormat.to(parseInt(values[0]));
	perMonth.innerHTML = outputFormat.to(calc);
	total.innerHTML = outputFormat.to(parseInt(values[0]));
})

var start = 0
var step = 100;
var days_min = 30;
var years_max = 4;
var start_suffix = ' dagar';
var end_suffix = ' år';
var period_slider = document.getElementById('period_slider');
noUiSlider.create(period_slider, {
	start: start,
	step: step,
	connect: [true, false],
	format: wNumb({
		decimals: 0
	}),
	range: {
		'min': start,
		'max': parseInt(years_max * step),
	},
	pips: {
		mode: 'range',
		density: parseInt(step / years_max),
		stepped: true,
		format: {
			to: toFormatPeriod
		}
	}
});
period_slider.noUiSlider.on('update', function (values) {
	var output = document.getElementById('period_value');
	var value = parseInt(values[0]);
	output.innerHTML = toFormatPeriod(value, true);
});

function toFormatPeriod(value, calculate) {
	if (value > start) {
		if (calculate === undefined) {
			return years_max + end_suffix;
		}
		return parseInt(value / step) + end_suffix;
	}
	return days_min + start_suffix;
}

function getElementY(query) {
	return window.pageYOffset + document.querySelector(query).getBoundingClientRect().top
}

function doScrolling(query, duration) {
	var startingY = window.pageYOffset
	var diff = getElementY(query) - startingY
	var start

	window.requestAnimationFrame(function step(timestamp) {
		if (!start) start = timestamp
		// Elapsed miliseconds since start of scrolling.
		var time = timestamp - start
		// Get percent of completion in range [0, 1].
		var percent = Math.min(time / duration, 1)

		window.scrollTo(0, startingY + diff * percent) // 0 + 973

		// Proceed with animation as long as we wanted it to.
		if (time < duration) {
			window.requestAnimationFrame(step)
		}
	})
}