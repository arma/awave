// Base Gulp File
var gulp = require('gulp'),
    less = require('gulp-less'),
    cssBase64 = require('gulp-css-base64'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    path = require('path'),
    notify = require('gulp-notify'),
    inlinesource = require('gulp-inline-source'),
    imagemin = require('gulp-imagemin'),
    del = require('del'),
    cache = require('gulp-cache'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    runSequence = require('run-sequence');

// Task to compile LESS
gulp.task('less', function () {
  return gulp.src('./src/less/styles.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ],
      sourceMap: {
        sourceMapRootpath: './src/less'
      }
    })
    .on("error", notify.onError(function(error) {
      return "Failed to Compile LESS: " + error.message;
    })))
    .pipe(cssBase64())
    .pipe(autoprefixer())
    .pipe(gulp.dest('./src/css/'))
    .pipe(notify("LESS Compiled Successfully :)"));
});

gulp.task('css', function() {
    return gulp.src('./src/css/*.css')
        .pipe(concat('styles.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('./dist/css/'))
        .pipe(notify("Css combined and cleaned Successfully :)"));
});

// Task to Minify JS
gulp.task('jsmin', function() {
  return gulp.src('./src/js/*.js')
    .pipe(concat('default.js'))
    .pipe(uglify())
    .pipe(rename('default.min.js'))
    .pipe(gulp.dest('./dist/js/'));
});

// Task to combine vendors
gulp.task('jsvendor', function() {
    return gulp.src('./src/js/vendor/*.js')
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest('./dist/js/'))
        .pipe(notify("Vendor js combined and uglified Successfully :)"));
});

// Minify Images
gulp.task('imagemin', function (){
  return gulp.src('./src/img/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('./dist/img/'));
});

// Gulp Inline Source Task
// Embed scripts, CSS or images inline (make sure to add an inline attribute to the linked files)
// Eg: <script src="default.js" inline></script>
// Will compile all inline within the html file (less http requests - woot!)
gulp.task('inlinesource', function () {
  return gulp.src('./src/**/*.html')
    .pipe(inlinesource())
    .pipe(gulp.dest('./dist/'));
});

// Gulp Clean Up Task
gulp.task('clean', function() {
    return del('dist');
});

// Gulp Default Task
gulp.task('default', ['build']);

// Compile less and process css
gulp.task('styles', function() {
    runSequence('less', 'css');
});

// Combine and uglify js
gulp.task('js', function() {
    runSequence('jsvendor', 'jsmin');
})

// Gulp Build Task
gulp.task('build', function() {
  runSequence('clean', 'styles', 'js', 'imagemin', 'inlinesource');
});